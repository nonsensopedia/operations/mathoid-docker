FROM node:16-bullseye

WORKDIR /mathoid

RUN set -eux; \
    # git and librsvg2-dev are already installed on the node image
    git clone --depth 1 -b master https://github.com/wikimedia/mathoid.git . ; \
    npm install

COPY config.yaml /mathoid/config.yaml
COPY entrypoint.sh /mathoid/entrypoint.sh

RUN chmod +x entrypoint.sh

ENTRYPOINT ["/mathoid/entrypoint.sh"]
